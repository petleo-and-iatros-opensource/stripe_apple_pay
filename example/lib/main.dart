import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:stripe_apple_pay/stripe_apple_pay.dart';
import 'package:stripe_apple_pay/stripe_apple_pay_status.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _deviceSupportsApplePay = false;
  StripeApplePay _stripeApplePay = StripeApplePay();

  @override
  void initState() {
    super.initState();
    _initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> _initPlatformState() async {
    _stripeApplePay.setDefaultPublishableKey('pk_test_TYooMQauvdEDq54NiTphI7jx');

    _stripeApplePay.setPaymentMethodHandler((paymentMethod) {
      return Future.delayed(
          Duration(seconds: 3), () => Future<String>.value('pi_DsUgbAYnghidwnWq2OULRr5S_secret_TljstS2C9LpmTCxKl3fFtnEHF'));
    });

    bool deviceSupportsApplePay;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      deviceSupportsApplePay = await _stripeApplePay.deviceSupportsApplePay;
    } on PlatformException {
      deviceSupportsApplePay = false;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _deviceSupportsApplePay = deviceSupportsApplePay;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: MyBody(deviceSupportsApplePay: _deviceSupportsApplePay, stripeApplePay: _stripeApplePay),
      ),
    );
  }
}

class MyBody extends StatelessWidget {
  final bool deviceSupportsApplePay;
  final StripeApplePay stripeApplePay;

  const MyBody({Key key, @required this.deviceSupportsApplePay, @required this.stripeApplePay}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(
        child: deviceSupportsApplePay
            ? RaisedButton(
                onPressed: () => _applePaySelected(context),
                child: Text('Initialize Apple Pay'),
              )
            : Text('Device doesn\'t support Apple Pay.'),
      ),
    );
  }

  _applePaySelected(BuildContext context) async {
    StripeApplePayStatus status = await stripeApplePay.initializeApplePay(
      'merchant.com.test_app_name',
      'DE',
      'EUR',
      [
        StripePaymentSummaryItem('Milk', 1.0),
        StripePaymentSummaryItem('Bread', 2.0),
        StripePaymentSummaryItem('Bananas', 4.0),
        StripePaymentSummaryItem('My Grocery Store', 7.0),
      ],
    );
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(status.toString())));
  }
}
