#import "StripeApplePayPlugin.h"
#if __has_include(<stripe_apple_pay/stripe_apple_pay-Swift.h>)
#import <stripe_apple_pay/stripe_apple_pay-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "stripe_apple_pay-Swift.h"
#endif

@implementation StripeApplePayPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftStripeApplePayPlugin registerWithRegistrar:registrar];
}
@end
