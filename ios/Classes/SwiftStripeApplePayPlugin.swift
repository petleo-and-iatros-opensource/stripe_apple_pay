import Flutter
import UIKit
import Stripe
import PassKit

public class SwiftStripeApplePayPlugin: NSObject, FlutterPlugin {

  static var result: FlutterResult?
  static var channel: FlutterMethodChannel?

  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "stripe_apple_pay", binaryMessenger: registrar.messenger())
    SwiftStripeApplePayPlugin.channel = channel
    let instance = SwiftStripeApplePayPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    switch call.method {
    case "setDefaultPublishableKey":
      _setDefaultPublishableKey(arguments: call.arguments, result: result, method: call.method)
    case "deviceSupportsApplePay":
      result(Stripe.deviceSupportsApplePay())
    case "initializeApplePay":
      SwiftStripeApplePayPlugin.result = result
      _initializeApplePay(arguments: call.arguments, result: result, method: call.method)
    default:
      result(FlutterError(code: "Error", message: "Method \(call.method) does not exist.", details: nil))
    }
  }

  private func _setDefaultPublishableKey(arguments: Any?, result: @escaping FlutterResult, method: String) {
    guard
      let defaultPublishableKey = arguments as? String
    else {
      result(FlutterError(code: "Error", message: "Wrong arguments for method \(method).", details: nil))
      return
    }

    Stripe.setDefaultPublishableKey(defaultPublishableKey)
  }

  private func _initializeApplePay(arguments: Any?, result: @escaping FlutterResult, method: String) {

    guard
      let arguments = arguments as? [String: Any],
      let merchantIdentifier = arguments["merchantIdentifier"] as? String,
      let country = arguments["country"] as? String,
      let currency = arguments["currency"] as? String,
      let paymentSummaryItems = arguments["paymentSummaryItems"] as? [[String: String]]
    else {
      result(FlutterError(code: "Error", message: "Wrong arguments for method \(method).", details: nil))
      return
    }

    let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: merchantIdentifier, country: country, currency: currency)
    paymentRequest.paymentSummaryItems = paymentSummaryItems.compactMap({ (item) -> PKPaymentSummaryItem? in
      guard let label = item["label"], let amount = item["amount"] else { return nil }
      return PKPaymentSummaryItem(label: label, amount: NSDecimalNumber(string: amount))
    })

    let flutterViewController = UIApplication.shared.keyWindow?.rootViewController as! FlutterViewController

    guard let applePayContext = STPApplePayContext(paymentRequest: paymentRequest, delegate: flutterViewController) else {
      result(FlutterError(code: "Error", message: "Something is wrong with Apple Pay configuration.", details: nil))
      return
    }
    applePayContext.presentApplePay(on: flutterViewController)
  }
}

extension FlutterViewController: STPApplePayContextDelegate {

  var result: FlutterResult? {
    SwiftStripeApplePayPlugin.result
  }

  enum StripeSecretError: Error {
    case failedFetchingSecret
  }

  public func applePayContext(_ context: STPApplePayContext, didCreatePaymentMethod paymentMethod: STPPaymentMethod, paymentInformation: PKPayment, completion: @escaping STPIntentClientSecretCompletionBlock) {
    SwiftStripeApplePayPlugin.channel?.invokeMethod("handlePaymentMethod", arguments: paymentMethod.stripeId, result: { (result) in
      guard let secret = result as? String else {
        completion(nil, StripeSecretError.failedFetchingSecret)
        return
      }
      completion(secret, nil)
    })
  }

  public func applePayContext(_ context: STPApplePayContext, didCompleteWith status: STPPaymentStatus, error: Error?) {
    switch status {
    case .success:
      result?("success")
      break
    case .error:
      result?("error")
      break
    case .userCancellation:
      result?("userCancellation")
      break
    @unknown default:
      fatalError()
    }
  }
}
