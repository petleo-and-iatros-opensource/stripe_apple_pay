import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:stripe_apple_pay/stripe_apple_pay_status.dart';

class StripeApplePay {
  final MethodChannel _channel = const MethodChannel('stripe_apple_pay');

  Future<bool?> get deviceSupportsApplePay async {
    if (!Platform.isIOS) {
      return false;
    }
    final bool? deviceSupportsApplePay =
        await _channel.invokeMethod('deviceSupportsApplePay');
    return deviceSupportsApplePay;
  }

  Future<void> setDefaultPublishableKey(String defaultPublishableKey) async {
    if (Platform.isIOS) {
      return await _channel.invokeMethod(
          'setDefaultPublishableKey', defaultPublishableKey);
    }
  }

  Future<StripeApplePayStatus?> initializeApplePay(
      String merchantIdentifier,
      String country,
      String currency,
      List<StripePaymentSummaryItem> paymentSummaryItems) async {
    if (!Platform.isIOS) {
      return StripeApplePayStatus.notSupported;
    }

    final paymentSummaryItemsMapped = paymentSummaryItems
        .map<Map<String, String>>(
            (e) => {'label': e.label, 'amount': e.amount.toString()})
        .toList();

    Map<String, dynamic> arguments = {
      'merchantIdentifier': merchantIdentifier,
      'country': country,
      'currency': currency,
      'paymentSummaryItems': paymentSummaryItemsMapped
    };

    final String? status =
        await _channel.invokeMethod('initializeApplePay', arguments);
    return StripeApplePayStatusHelper.create(status);
  }

  setPaymentMethodHandler(
      Future<String> Function(String?) paymentMethodHandler) {
    _channel.setMethodCallHandler((call) {
      if (call.method != "handlePaymentMethod") {
        return Future.error(Error());
      }

      final String? paymentMethod = call.arguments;
      return paymentMethodHandler(paymentMethod);
    });
  }
}

class StripePaymentSummaryItem {
  final String label;
  final double amount;

  StripePaymentSummaryItem(this.label, this.amount);
}
