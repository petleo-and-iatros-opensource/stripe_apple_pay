import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';

enum StripeApplePayStatus { success, error, userCancellation, notSupported }

class StripeApplePayStatusHelper {
  static StripeApplePayStatus? create(String? string) {
    return StripeApplePayStatus.values
        .firstWhereOrNull((e) => describeEnum(e) == string);
  }
}
